package userhdl

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"gitlab.com/ingestevezaquino/golang-api/internal/core/services/usersrv"
	"gitlab.com/ingestevezaquino/golang-api/internal/infrastructure/server/dtos/user_dto"
	"gitlab.com/ingestevezaquino/golang-api/internal/infrastructure/server/response"
)

type userhdl struct {
	service *usersrv.Service
}

func NewUserHandler(service *usersrv.Service) *userhdl {
	return &userhdl{service}
}

func (userhdl *userhdl) GetById(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		resp := response.New(false, http.StatusBadRequest, "Invalid Method", nil)
		response.ResponseJSON(w, resp)
		return
	}

	userID, err := strconv.ParseUint(r.URL.Query().Get("id"), 10, 0)
	if err != nil {
		resp := response.New(false, http.StatusBadRequest, "The specified ID must be a positive number", nil)
		response.ResponseJSON(w, resp)
		return
	}

	user, err := userhdl.service.GetById(userID)
	if err != nil {
		resp := response.New(false, http.StatusNotFound, "The specified ID does not exist in the database", nil)
		response.ResponseJSON(w, resp)
		return
	}

	resp := response.New(true, http.StatusOK, "OK", user_dto.BuildResponseUserGet(user))
	response.ResponseJSON(w, resp)
}

func (userhdl *userhdl) GetAll(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		resp := response.New(false, http.StatusBadRequest, "Invalid Method", nil)
		response.ResponseJSON(w, resp)
		return
	}

	users, err := userhdl.service.GetAll()
	if err != nil {
		resp := response.New(false, http.StatusInternalServerError, fmt.Sprintf(`{
			"successful": false,
			"status_code": %d,
			"message": %v,
			"data": null
		}`, http.StatusInternalServerError, err), nil)
		response.ResponseJSON(w, resp)
		return
	}

	responseUsersGet := user_dto.BuildResponseUsersGet(users)
	resp := response.New(true, http.StatusOK, "OK", responseUsersGet)
	response.ResponseJSON(w, resp)
}

func (userhdl *userhdl) Create(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		resp := response.New(false, http.StatusBadRequest, "Invalid Method", nil)
		response.ResponseJSON(w, resp)
		return
	}

	responseUserPost := &user_dto.ResponseUserPost{}
	err := json.NewDecoder(r.Body).Decode(responseUserPost)
	if err != nil {
		resp := response.New(false, http.StatusBadRequest, "The USER object was bad constructed", nil)
		response.ResponseJSON(w, resp)
		return
	}

	err = userhdl.service.Create(user_dto.BuildResponseUserPost(*responseUserPost))
	if err != nil {
		resp := response.New(false, http.StatusInternalServerError, fmt.Sprintf(`{
			"successful": false,
			"status_code": %d,
			"message": %v,
			"data": null
		}`, http.StatusInternalServerError, err), nil)
		response.ResponseJSON(w, resp)
		return
	}

	resp := response.New(true, http.StatusCreated, "OK", nil)
	response.ResponseJSON(w, resp)
}
