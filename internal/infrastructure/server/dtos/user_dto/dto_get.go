package user_dto

import "gitlab.com/ingestevezaquino/golang-api/internal/core/domain"

type ResponseUserGet struct {
	ID   uint64 `json:"id"`
	Name string `json:"name"`
}

type ResponseUsersGet []ResponseUserGet

func BuildResponseUserGet(user domain.User) *ResponseUserGet {
	responseUserGet := &ResponseUserGet{}
	responseUserGet.ID = user.ID
	responseUserGet.Name = user.Name
	return responseUserGet
}

func BuildResponseUsersGet(users domain.Users) []*ResponseUserGet {
	responseUsersGet := make([]*ResponseUserGet, 0)
	for _, user := range users {
		responseUsersGet = append(responseUsersGet, &ResponseUserGet{ID: user.ID, Name: user.Name})
	}
	return responseUsersGet
}
