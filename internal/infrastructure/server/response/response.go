package response

import (
	"encoding/json"
	"fmt"
	"net/http"
)

type response struct {
	Successful bool        `json:"successful"`
	StatusCode int         `json:"status_code"`
	Message    string      `json:"message"`
	Data       interface{} `json:"data"`
}

func New(successful bool, statusCode int, message string, data interface{}) *response {
	return &response{successful, statusCode, message, data}
}

func ResponseJSON(w http.ResponseWriter, r *response) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(r.StatusCode)
	err := json.NewEncoder(w).Encode(r)
	if err != nil {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(fmt.Sprintf(`{
			"successful": false,
			"status_code": %d,
			"message": %v,
			"data": null
		}`, http.StatusInternalServerError, err)))
	}
}
