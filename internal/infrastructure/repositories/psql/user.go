package psql

import (
	"database/sql"
	"log"

	"gitlab.com/ingestevezaquino/golang-api/internal/core/domain"
)

const (
	psqlGetUserById = "SELECT id, name, created_at FROM users WHERE id = $1"
	psqlGetAllUsers = "SELECT id, name, created_at FROM users"
	psqlInsertUser  = "INSERT INTO users (id, name, created_at) VALUES (DEFAULT, $1, $2) RETURNING id"
)

type userRepository struct {
	db *sql.DB
}

func NewUserRepository(db *sql.DB) *userRepository {
	return &userRepository{db}
}

func (u *userRepository) Get(id uint64) (domain.User, error) {
	stmt, err := u.db.Prepare(psqlGetUserById)
	if err != nil {
		return domain.User{}, err
	}
	defer stmt.Close()

	user := domain.User{}
	err = stmt.QueryRow(id).Scan(&user.ID, &user.Name, &user.CreatedAt)
	if err != nil {
		return domain.User{}, err
	}

	return user, nil
}

func (u *userRepository) GetAll() (domain.Users, error) {
	stmt, err := u.db.Prepare(psqlGetAllUsers)
	if err != nil {
		return domain.Users{}, err
	}
	defer stmt.Close()

	rows, err := stmt.Query()
	if err != nil {
		return domain.Users{}, err
	}

	users := make(domain.Users, 0)
	for rows.Next() {
		user := domain.User{}
		err = rows.Scan(
			&user.ID,
			&user.Name,
			&user.CreatedAt,
		)

		if err != nil {
			return domain.Users{}, err
		}
		users = append(users, user)
	}

	if err = rows.Err(); err != nil {
		return domain.Users{}, err
	}

	return users, nil
}

func (u *userRepository) Save(user *domain.User) error {
	stmt, err := u.db.Prepare(psqlInsertUser)
	if err != nil {
		return err
	}
	defer stmt.Close()

	err = stmt.QueryRow(user.Name, user.CreatedAt).Scan(&user.ID)
	if err != nil {
		return err
	}

	log.Printf("User was added successfully with ID: %d\n", user.ID)
	return nil
}
