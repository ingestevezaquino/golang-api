package usersrv

import (
	"time"

	"gitlab.com/ingestevezaquino/golang-api/internal/core/domain"
	"gitlab.com/ingestevezaquino/golang-api/internal/core/ports"
)

type Service struct {
	repo ports.UserRepository
}

func NewService(repo ports.UserRepository) *Service {
	return &Service{repo}
}

func (s *Service) GetById(id uint64) (domain.User, error) {
	return s.repo.Get(id)
}

func (s *Service) GetAll() (domain.Users, error) {
	return s.repo.GetAll()
}

func (s *Service) Create(user *domain.User) error {
	user.CreatedAt = time.Now()
	return s.repo.Save(user)
}
