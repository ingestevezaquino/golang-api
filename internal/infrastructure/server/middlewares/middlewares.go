package middlewares

import (
	"log"
	"net/http"
	"time"

	"gitlab.com/ingestevezaquino/golang-api/internal/infrastructure/server/response"
)

type handlerFunc = func(http.ResponseWriter, *http.Request)

func Log(h handlerFunc) handlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		timeStart := time.Now()
		log.Printf("Request: %q, Method: %q\n", r.URL.Path, r.Method)
		h(w, r)
		log.Printf("Elapsed time since the request was received: %v", time.Since(timeStart))
	}
}

func Authentication(h handlerFunc) handlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		token := r.Header.Get("Authorization")
		if token != "un-token-muy-seguro" {
			resp := response.New(false, http.StatusForbidden, "Access denied. Please, make sure you are sending a valid token", nil)
			response.ResponseJSON(w, resp)
			return
		}

		h(w, r)
	}
}
