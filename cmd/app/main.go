package main

import (
	"log"
	"net/http"

	"gitlab.com/ingestevezaquino/golang-api/internal/infrastructure/repositories/storage"
	"gitlab.com/ingestevezaquino/golang-api/internal/infrastructure/server"
)

func main() {
	storage.OpenConnection("postgres")
	mux := http.NewServeMux()
	server.RoutesInit(mux)

	log.Println("Server is running at http://localhost:8080")
	err := http.ListenAndServe(":8080", mux)
	if err != nil {
		log.Fatalf("Server stopped, Error: %v\n", err)
	}
}
