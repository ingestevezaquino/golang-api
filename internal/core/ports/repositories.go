package ports

import "gitlab.com/ingestevezaquino/golang-api/internal/core/domain"

type UserRepository interface {
	Get(id uint64) (domain.User, error)
	GetAll() (domain.Users, error)
	Save(*domain.User) error
}
