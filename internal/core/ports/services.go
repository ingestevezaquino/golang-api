package ports

import "gitlab.com/ingestevezaquino/golang-api/internal/core/domain"

type UserService interface {
	GetById(id uint64) (domain.User, error)
	GetAll() (domain.Users, error)
	Create(*domain.User) error
}
