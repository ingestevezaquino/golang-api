package user_dto

import "gitlab.com/ingestevezaquino/golang-api/internal/core/domain"

type ResponseUserPost struct {
	Name string `json:"name"`
}

func BuildResponseUserPost(rup ResponseUserPost) *domain.User {
	user := &domain.User{}
	user.Name = rup.Name
	return user
}
