package storage

import (
	"database/sql"
	"log"
	"os"
	"strings"
	"sync"

	_ "github.com/lib/pq"
)

var (
	db   *sql.DB
	once sync.Once
)

func OpenConnection(engine string) {
	once.Do(func() {
		var conn_string string
		var err error

		switch {
		case strings.ToLower(engine) == "postgres":
			conn_string = os.Getenv("PsqlConnString")
		}

		db, err = sql.Open(engine, conn_string)
		if err != nil {
			log.Fatalf("Connection with the database couldn't be established, error: %v\n", err)
		}

		if err = db.Ping(); err != nil {
			log.Fatalf("Connection with the database couldn't be established, error: %v\n", err)
		}
	})
}

func Pool() *sql.DB {
	return db
}
