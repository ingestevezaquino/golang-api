package server

import (
	"net/http"

	"gitlab.com/ingestevezaquino/golang-api/internal/core/services/usersrv"
	"gitlab.com/ingestevezaquino/golang-api/internal/infrastructure/repositories/psql"
	"gitlab.com/ingestevezaquino/golang-api/internal/infrastructure/repositories/storage"
	"gitlab.com/ingestevezaquino/golang-api/internal/infrastructure/server/handlers/userhdl"
	"gitlab.com/ingestevezaquino/golang-api/internal/infrastructure/server/middlewares"
)

func RoutesInit(mux *http.ServeMux) {
	userrepo := psql.NewUserRepository(storage.Pool())
	usersrv := usersrv.NewService(userrepo)
	userhdl := userhdl.NewUserHandler(usersrv)

	mux.HandleFunc("/api/v1/users/get", middlewares.Log(userhdl.GetById))
	mux.HandleFunc("/api/v1/users/get-all", middlewares.Log(middlewares.Authentication(userhdl.GetAll)))
	mux.HandleFunc("/api/v1/users/create", middlewares.Log(userhdl.Create))
}
